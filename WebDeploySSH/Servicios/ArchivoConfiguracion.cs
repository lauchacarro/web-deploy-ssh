﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDeploySSH.DTO;

namespace WebDeploySSH.Servicios
{
    public class ArchivoConfiguracion
    {

        private const string nombreArchivo = "WebDeploySsh.json";


        public static void CrearArchivoConfiguracion(PublicacionConfig publicacionConfig)
        {
            publicacionConfig.Servidor.Contraseña = string.Empty;

            FileInfo fileArchivoProyecto = new FileInfo(publicacionConfig.ArchivoProyectoPath);
            string pathConfigPath = Path.Combine(fileArchivoProyecto.Directory.FullName, nombreArchivo);
            if (File.Exists(pathConfigPath))
            {
                File.Delete(pathConfigPath);
            }

            using (StreamWriter file = File.CreateText(pathConfigPath))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, publicacionConfig);
            }



        }

        public static bool ExisteArhchivoConfiguracion(DirectoryInfo directoryProyect)
        {
            return File.Exists(Path.Combine(directoryProyect.FullName, nombreArchivo));
        }

        public static PublicacionConfig ObtenerConfiguracion(DirectoryInfo directoryProyect)
        {
            FileInfo archivoConfiguracion = new FileInfo(Path.Combine(directoryProyect.FullName, nombreArchivo));

            // deserialize JSON directly from a file
            using (StreamReader file = archivoConfiguracion.OpenText())
            {
                JsonSerializer serializer = new JsonSerializer();
                PublicacionConfig config = (PublicacionConfig)serializer.Deserialize(file, typeof(PublicacionConfig));
                return config;
            }

        }

    }
}
