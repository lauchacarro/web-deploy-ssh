﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDeploySSH.DTO;

namespace WebDeploySSH.Servicios
{
    public class SshUtilities
    {
        private Servidor Servidor { get; set; }

        public SshUtilities(Servidor servidor)
        {
            Servidor = servidor;
        }

        public void EjecutarComandos(string comando, bool isSudo)
        {
            using (SshClient client = new SshClient(Servidor.ServidorAddress, Servidor.Usuario, Servidor.Contraseña))
            {
                client.Connect();

                EjecutarComando(client, comando, isSudo);

                client.Disconnect();
            }
        }

        public void EjecutarComandos(IEnumerable<string> comandos, bool isSudo)
        {
            using (SshClient client = new SshClient(Servidor.ServidorAddress, Servidor.Usuario, Servidor.Contraseña))
            {
                client.Connect();
                foreach (string comando in comandos)
                {
                    EjecutarComando(client, comando, isSudo);

                }

                client.Disconnect();
            }
        }

        public bool ExisteServicio(string nombreServicio)
        {
            //systemctl list-unit-files --type=service | grep  "kestrel-HotelVallddeDelVolcan.service" 
            using (SshClient client = new SshClient(Servidor.ServidorAddress, Servidor.Usuario, Servidor.Contraseña))
            {
                client.Connect();

                SshCommand resultado = EjecutarComando(client, $"systemctl list-unit-files --type=service | grep  '{nombreServicio}' | grep enabled && echo '' || echo ''", false);

                client.Disconnect();

                return resultado.Result.Trim().Contains(nombreServicio);
            }
        }

        public void BorrarArchivoExistente(string fullPathFile)
        {
            string comandoSimple = $"rm '{fullPathFile}'";
            string comandoCompleto = $"[ -e '{fullPathFile}' ] && {ComandoSudo(comandoSimple)} || echo ''";
            using (SshClient client = new SshClient(Servidor.ServidorAddress, Servidor.Usuario, Servidor.Contraseña))
            {
                client.Connect();

                EjecutarComando(client, comandoCompleto, false);

                client.Disconnect();
            }
        }

        public void CrearDirectorioNoExistente(string fullPath)
        {
            using (SshClient client = new SshClient(Servidor.ServidorAddress, Servidor.Usuario, Servidor.Contraseña))
            {
                client.Connect();

                List<string> carpetas = fullPath.Split('/').ToList();

                if (carpetas[0].Trim() == "/" || string.IsNullOrWhiteSpace(carpetas[0].Trim()))
                {
                    carpetas.RemoveAt(0);
                }

                for (int i = 0; i < carpetas.Count(); i++)
                {
                    string carpeta = string.Empty;

                    for (int k = 0; k <= i; k++)
                    {
                        carpeta = carpeta + "/" + carpetas[k];
                    }
                    string comandoSimple = $"mkdir '{carpeta}'";
                    string comandoCompleto = $"[ -e '{carpeta}' ] && echo '' || {ComandoSudo(comandoSimple)}";

                    EjecutarComando(client, comandoCompleto, false);

                    
                }
                EjecutarComando(client, $"chmod -R 777 '{fullPath}'", true);
                client.Disconnect();
            }


        }

        private string ComandoSudo(string comandoSimple)
        {
            return $"echo '{Servidor.Contraseña}\n' | sudo -S {comandoSimple}";
        }

        private SshCommand EjecutarComando(SshClient client, string comando, bool isSudo)
        {
            SshCommand resultado;
            if (isSudo)
            {
                resultado = client.RunCommand(ComandoSudo(comando));
            }
            else
            {
                resultado = client.RunCommand(comando);
            }

            if (resultado.ExitStatus == 1)
            {
                client.Disconnect();
                throw new Exception(resultado.Error);
            }
            return resultado;
        }
    }
}
