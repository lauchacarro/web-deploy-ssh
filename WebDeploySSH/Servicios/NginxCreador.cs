﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDeploySSH.DTO;

namespace WebDeploySSH.Servicios
{
    public class NginxCreador : ICreador
    {
        
        private Servidor Servidor { get; set; }

        private PublicacionConfig PublicacionConfig { get; set; }

        public NginxCreador(Servidor servidor, PublicacionConfig publicacionConfig)
        {
            Servidor = servidor;
            PublicacionConfig = publicacionConfig;
        }


        public FileInfo CrearLocal()
        {
            string[] urls = PublicacionConfig.ConfiguracionAvanzada.Urls;
            string urlMain = urls[0];
            string lineaDeUrls = string.Empty;
            foreach (string url in urls)
            {
                lineaDeUrls = lineaDeUrls + url + " ";
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("server {");
            sb.AppendLine("listen        80;");
            sb.AppendLine($"server_name {lineaDeUrls};");
            sb.AppendLine("location / {");
            sb.AppendLine($"    proxy_pass http://localhost:{PublicacionConfig.ConfiguracionAvanzada.Puerto};");
            sb.AppendLine("     proxy_http_version 1.1;");
            sb.AppendLine("     proxy_set_header Upgrade $http_upgrade;");
            sb.AppendLine("     proxy_set_header Connection keep-alive;");
            sb.AppendLine("     proxy_set_header Host $host;");
            sb.AppendLine("     proxy_cache_bypass $http_upgrade;");
            sb.AppendLine("     proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;");
            sb.AppendLine("     proxy_set_header X-Forwarded-Proto $scheme;");
            sb.AppendLine("     }");
            sb.AppendLine("}");

            string pathCompleto = Environment.CurrentDirectory + "/" + urlMain;
            File.Create(pathCompleto).Dispose();

            File.WriteAllText(pathCompleto, sb.ToString());


            return new FileInfo(pathCompleto); ;
        }

        public void SubirAlServidor(FileInfo fileInfo)
        {
            SshUtilities sshUtilities = new SshUtilities(Servidor);

            sshUtilities.BorrarArchivoExistente($"/var/tmp/{fileInfo.Name}");
            sshUtilities.BorrarArchivoExistente($"/etc/nginx/sites-enabled/{fileInfo.Name}");
            sshUtilities.BorrarArchivoExistente($"/etc/nginx/sites-available/{fileInfo.Name}");
            SubirArchivo subirArchivo = new SubirArchivo(Servidor);

            subirArchivo.PathRemote = "/var/tmp/";
            subirArchivo.Archivo = fileInfo;

            string pathArchivoRemoto = subirArchivo.Subir();

            
            
            string[] comandos = new string[]
            {
                $"mv '{pathArchivoRemoto}' '/etc/nginx/sites-available/{fileInfo.Name}'",
                $"ln -s '/etc/nginx/sites-available/{fileInfo.Name}' '/etc/nginx/sites-enabled/{fileInfo.Name}'",
                "service nginx restart"
            };

            sshUtilities.EjecutarComandos(comandos, true);

        
        }



        //private int ObtenerPuerto()
        //{
        //    FileInfo file = new FileInfo(PublicacionConfig.ArchivoProyectoPath);
        //    string path = file.DirectoryName;
        //    string content = File.ReadAllText(Path.Combine(path, "hosting.json"));
        //    string puertoString = content.Split(':').Last();
        //    string puerto = string.Empty;
        //    foreach (char item in puertoString)
        //    {
        //        if (int.TryParse(item.ToString(), out int result))
        //        {
        //            puerto = puerto + item.ToString();
        //        }
        //        else
        //        {
        //            break;
        //        }
        //    }

        //    return Convert.ToInt32(puerto);
        //}
    }
}
