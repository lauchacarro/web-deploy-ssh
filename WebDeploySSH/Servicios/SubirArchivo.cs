﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDeploySSH.DTO;

namespace WebDeploySSH.Servicios
{
    public class SubirArchivo
    {

        private Servidor Servidor { get; set; }

        public FileInfo Archivo { get; set; }

        public string PathRemote { get; set; } 

        public SubirArchivo(Servidor servidor)
        {
            Servidor = servidor;
        }
        /// <summary>
        /// Subo el archivo al servidor en el path especificado y devuelve la direccion remota del archivo subido
        /// </summary>
        public string Subir()
        {
            using (ScpClient client = new ScpClient(Servidor.ServidorAddress, Servidor.Usuario, Servidor.Contraseña))
            {
                client.RemotePathTransformation = RemotePathTransformation.ShellQuote;
                client.Connect();

                client.Upload(Archivo, PathRemote + Archivo.Name);

                client.Disconnect();

                return PathRemote + Archivo.Name;
            }
        }
    }
}
