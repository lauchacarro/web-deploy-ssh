﻿using Renci.SshNet;
using Renci.SshNet.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDeploySSH.DTO;

namespace WebDeploySSH.Servicios
{
    public class PublicacionProyecto
    {
        private PublicacionConfig Publicacion { get; set; }

        //private Servidor Servidor { get; set; }

        public PublicacionProyecto(PublicacionConfig publicacion)
        {
            Publicacion = publicacion;
        }

        public void CompilarProyecto()
        {
            Process programa = new Process();
            ProcessStartInfo info = new ProcessStartInfo($"dotnet")
            {
                Arguments = $"publish -c release \"{Publicacion.ArchivoProyectoPath}\" -o \"{Publicacion.PathOutputLocal}\"",
                WindowStyle = ProcessWindowStyle.Hidden
            };
            programa = Process.Start(info);

            programa.WaitForExit();

        }

        public void PublicarProyecto()
        {
            if (Publicacion.ProyectoTipo == Enums.ProyectoTipoEnum.NPM)
            {
                if (Directory.Exists($"{Publicacion.PathOutputLocal}/temp"))
                {
                    Directory.Delete($"{Publicacion.PathOutputLocal}/temp", true);
                }
                DirectoryCopy(Publicacion.PathOutputLocal, $"{Publicacion.PathOutputLocal}/temp", true);

                
            }
            DirectoryInfo directoryInfo = new DirectoryInfo($"{Publicacion.PathOutputLocal}/temp");
            SshUtilities sshUtilities = new SshUtilities(Publicacion.Servidor);
            sshUtilities.CrearDirectorioNoExistente(Publicacion.PathOutputRemota);
            using (ScpClient client = new ScpClient(Publicacion.Servidor.ServidorAddress, Publicacion.Servidor.Usuario, Publicacion.Servidor.Contraseña))
            {
                client.RemotePathTransformation = RemotePathTransformation.ShellQuote;
                client.Connect();

                client.Upload(directoryInfo, Publicacion.PathOutputRemota);

                client.Disconnect();
            }
            if (Directory.Exists($"{Publicacion.PathOutputLocal}/temp"))
            {
                Directory.Delete($"{Publicacion.PathOutputLocal}/temp", true);
            }

        }

        public void EliminarArchivosLocales()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(Publicacion.PathOutputLocal);
            directoryInfo.Delete(true);
            directoryInfo.Create();
        }

        public void EliminarArchivosRemotos()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(Publicacion.PathOutputLocal);
            SshUtilities sshUtilities = new SshUtilities(Publicacion.Servidor);

            string[] comandos = new string[]
            {
                $"rm -rf {Publicacion.PathOutputRemota}"
            };

            sshUtilities.EjecutarComandos(comandos, true);



        }



        internal void CrearArchivoNginx()
        {
            NginxCreador nginxUtilitis = new NginxCreador(Publicacion.Servidor, Publicacion);
            FileInfo archivoNginx = nginxUtilitis.CrearLocal();
            nginxUtilitis.SubirAlServidor(archivoNginx);


        }

        public void SubirArchivo()
        {
            SshUtilities sshUtilities = new SshUtilities(Publicacion.Servidor);
            //string preComando = $"cat /etc/nginx/sites-available/ubu5ntu";
            sshUtilities.BorrarArchivoExistente("/usr/local/bin/caca.txt");
        }




        public void CrearServicio()
        {
            StartScriptCreador startScriptCreador = new StartScriptCreador(Publicacion.Servidor, Publicacion);
            FileInfo archivoScript = startScriptCreador.CrearLocal();
            startScriptCreador.SubirAlServidor(archivoScript);

            DemonioCreador demonioCreador = new DemonioCreador(Publicacion.Servidor, Publicacion);
            FileInfo archivoDemonio = demonioCreador.CrearLocal();
            demonioCreador.SubirAlServidor(archivoDemonio);


        }


        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    if(subdir.Name != ".git" && subdir.Name != "node_modules")
                    {
                        string temppath = Path.Combine(destDirName, subdir.Name);
                        DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                    }
                    
                }
            }
        }
    }
}
