﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDeploySSH.DTO;

namespace WebDeploySSH.Servicios
{
    public class DemonioCreador : ICreador
    {
        private Servidor Servidor { get; set; }

        private PublicacionConfig PublicacionConfig { get; set; }

        public DemonioCreador(Servidor servidor, PublicacionConfig publicacionConfig)
        {
            Servidor = servidor;
            PublicacionConfig = publicacionConfig;
        }

        public FileInfo CrearLocal()
        {
            string nombreProyecto = string.Empty;

            if (PublicacionConfig.ProyectoTipo == Enums.ProyectoTipoEnum.NetCore)
                nombreProyecto = Path.GetFileNameWithoutExtension(PublicacionConfig.ArchivoProyectoPath).Replace(" ", "-");
            if (PublicacionConfig.ProyectoTipo == Enums.ProyectoTipoEnum.NPM)
            {
                string dirName = new DirectoryInfo(PublicacionConfig.PathOutputLocal).Name;
                nombreProyecto = dirName.Replace(" ", "-");
            }
            nombreProyecto = PublicacionConfig.ConfiguracionAvanzada.Ambiente.ToString() + "-" + nombreProyecto;
            StringBuilder sb = new StringBuilder();
            //tr -d '\r' < infile > outfile
            sb.AppendLine("[Unit]");
            sb.AppendLine($"Description=Iniciar {nombreProyecto}");
            sb.AppendLine("After=networking.target");
            sb.AppendLine();
            sb.AppendLine("[Service]");
            sb.AppendLine("Type=simple");
            sb.AppendLine($"ExecStart=/usr/local/bin/{nombreProyecto}Start.sh");
            sb.AppendLine();
            sb.AppendLine("Restart=always");
            sb.AppendLine("RestartSec=10");
            sb.AppendLine("StartLimitBurst=5");
            sb.AppendLine("StartLimitInterval=600");
            sb.AppendLine();
            sb.AppendLine("[Install]");
            sb.AppendLine("WantedBy=multi-user.target");

            string pathCompleto = Environment.CurrentDirectory + "/kestrel-" + nombreProyecto + ".service";

            File.Create(pathCompleto).Dispose();
            File.WriteAllText(pathCompleto, sb.ToString());

            return new FileInfo(pathCompleto);
        }

        public void SubirAlServidor(FileInfo fileInfo)
        {
            string[] comandos = new string[] { };
            SshUtilities sshUtilities = new SshUtilities(Servidor);


            sshUtilities.BorrarArchivoExistente($"/var/tmp/{fileInfo.Name}");


            if (sshUtilities.ExisteServicio(fileInfo.Name))
            {
                comandos = new string[]
                {
                    $"systemctl daemon-reload",
                    $"systemctl stop {fileInfo.Name}",
                    $"systemctl disable {fileInfo.Name}"
                };
                sshUtilities.EjecutarComandos(comandos, true);
                sshUtilities.BorrarArchivoExistente($"/etc/systemd/system/{fileInfo.Name}");

            }

            
            



            SubirArchivo subirArchivo = new SubirArchivo(Servidor);
            subirArchivo.Archivo = fileInfo;
            subirArchivo.PathRemote = "/var/tmp/";


            string pathArchivoRemoto = subirArchivo.Subir();


            comandos = new string[]
{
                $"mv {pathArchivoRemoto} /etc/systemd/system/{fileInfo.Name}",
                $"systemctl daemon-reload",
                $"systemctl start {fileInfo.Name}",
                $"systemctl enable {fileInfo.Name}"
};

            sshUtilities.EjecutarComandos(comandos, true);
        }
    }
}
