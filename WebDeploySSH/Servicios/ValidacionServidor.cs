﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDeploySSH.DTO;

namespace WebDeploySSH.Servicios
{
    public class ValidacionServidor
    {
        public static bool Validar(Servidor servidor, out string msgError)
        {
            msgError = null;
            try
            {
                using (ScpClient client = new ScpClient(servidor.ServidorAddress, servidor.Usuario, servidor.Contraseña))
                {
                    client.RemotePathTransformation = RemotePathTransformation.ShellQuote;
                    client.Connect();
                    client.Disconnect();
                }
                return true;
            }
            catch (Exception ex )
            {
                msgError = ex.Message;
                return false;
            }
            
                
        }
    }
}
