﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDeploySSH.DTO;

namespace WebDeploySSH.Servicios
{
    public class StartScriptCreador : ICreador
    {

        private Servidor Servidor { get; set; }

        private PublicacionConfig PublicacionConfig { get; set; }

        public StartScriptCreador(Servidor servidor, PublicacionConfig publicacionConfig)
        {
            Servidor = servidor;
            PublicacionConfig = publicacionConfig;
        }
    
        public FileInfo CrearLocal()
        {
            string nombreProyecto = string.Empty;

            if (PublicacionConfig.ProyectoTipo == Enums.ProyectoTipoEnum.NetCore)
                nombreProyecto = Path.GetFileNameWithoutExtension(PublicacionConfig.ArchivoProyectoPath).Replace(" ", "-");
            if (PublicacionConfig.ProyectoTipo == Enums.ProyectoTipoEnum.NPM)
            {
                string dirName = new DirectoryInfo(PublicacionConfig.PathOutputLocal).Name;
                nombreProyecto = dirName.Replace(" ", "-");
            }
                


            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine("#!/bin/bash");
            sbScript.AppendLine($"cd {PublicacionConfig.PathOutputRemota}");

            if(PublicacionConfig.ProyectoTipo == Enums.ProyectoTipoEnum.NetCore)
                sbScript.AppendLine($"/usr/bin/dotnet {nombreProyecto}.dll");
            if (PublicacionConfig.ProyectoTipo == Enums.ProyectoTipoEnum.NPM)
            {
                sbScript.AppendLine($"/usr/bin/npm install");
                sbScript.AppendLine($"/usr/bin/npm start");
            }





            string pathCompleto = Environment.CurrentDirectory + "/" + PublicacionConfig.ConfiguracionAvanzada.Ambiente.ToString() + "-" + nombreProyecto + "Start";
            
            File.Create(pathCompleto).Dispose();
            File.WriteAllText(pathCompleto, sbScript.ToString());

            return new FileInfo(pathCompleto);
        }

        public void SubirAlServidor(FileInfo fileInfo)
        {
            SshUtilities sshUtilities = new SshUtilities(Servidor);
            sshUtilities.BorrarArchivoExistente($"/var/tmp/{fileInfo.Name}");
            sshUtilities.BorrarArchivoExistente($"/var/tmp/{fileInfo.Name}.sh");
            sshUtilities.BorrarArchivoExistente($"/usr/local/bin/{fileInfo.Name}.sh");



            SubirArchivo subirArchivo = new SubirArchivo(Servidor);

            subirArchivo.PathRemote = "/var/tmp/";
            subirArchivo.Archivo = fileInfo;

            string pathArchivoRemoto = subirArchivo.Subir();

            
            string preComando = $"tr -d '\r' < {pathArchivoRemoto} > {pathArchivoRemoto}.sh";
            sshUtilities.EjecutarComandos(preComando, false);


            string[] comandos = new string[]
            {
 
                $"mv {pathArchivoRemoto}.sh /usr/local/bin/{fileInfo.Name}.sh",
                $"chmod +x /usr/local/bin/{fileInfo.Name}.sh"
            };

            sshUtilities.EjecutarComandos(comandos, true);
        }
    }
}
