﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDeploySSH
{
    public interface ICreador
    {
        FileInfo CrearLocal();

        void SubirAlServidor(FileInfo fileInfo);
    }
}
