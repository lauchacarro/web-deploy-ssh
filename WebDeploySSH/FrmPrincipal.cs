﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebDeploySSH.DTO;
using WebDeploySSH.Enums;
using WebDeploySSH.Servicios;

namespace WebDeploySSH
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
            PublicacionConfig = new PublicacionConfig();
        }

        PublicacionConfig PublicacionConfig { get; set; }

        private Servidor CrearServidor()
        {
            return new Servidor()
            {
                ServidorAddress = txtServidor.Text,
                Usuario = txtUsuario.Text,
                Contraseña = txtContraseña.Text
            };
        }

        private void NewPublicacionConfig(ref PublicacionConfig publicacionConfig)
        {

            publicacionConfig.ArchivoProyectoPath = txtPathArchivo.Text;
            publicacionConfig.PathOutputLocal = txtCarpetaOutput.Text;
            publicacionConfig.PathOutputRemota = txtCarpetaRemota.Text;
            publicacionConfig.EliminarExistenteLocal = chkEliminarArchivosCarpetaOutput.Checked;
            publicacionConfig.EliminarExistenteRemoto = chkEliminarArchivosCarpetaRemota.Checked;
            publicacionConfig.Servidor = CrearServidor();
            publicacionConfig.ProyectoTipo = (ProyectoTipoEnum)Enum.Parse(typeof(ProyectoTipoEnum), cboTipoProyecto.Text);


        }


        private void BtnValidarConexion_Click(object sender, EventArgs e)
        {
            PublicacionConfig.Servidor = CrearServidor();
            if (ValidacionServidor.Validar(PublicacionConfig.Servidor, out string msgError))
            {
                MessageBox.Show("Se Conecto Correctamente", "Validar Conexion", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show(msgError, "Validar Conexion", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void BtnSeleccionarArchivo_Click(object sender, EventArgs e)
        {
            fileDialog.Filter = "Archivo de proyecto|*.csproj";
            fileDialog.Title = "Seleccione el archivo .csproj";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {

                FileInfo fileInfo = new FileInfo(fileDialog.FileName);
                txtPathArchivo.Text = fileInfo.FullName;
                ObtenerArchivoConfiguracion(fileInfo.Directory);
            }
        }

        private void ObtenerArchivoConfiguracion(DirectoryInfo directoryInfo)
        {
            if (ArchivoConfiguracion.ExisteArhchivoConfiguracion(directoryInfo))
            {
                DialogResult respuesta = MessageBox.Show("Se encontro un archivo de configuracion \r\n ¿Desea recuperar la configuracion encontrada? ", "Archivo Configuracion", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (respuesta == DialogResult.Yes)
                {
                    PublicacionConfig publicacionConfig = ArchivoConfiguracion.ObtenerConfiguracion(directoryInfo);
                    CompletarCampos(publicacionConfig);
                }
            }
        }

        private void GuardarArchivoConfiguracion(DirectoryInfo directoryInfo)
        {
            DialogResult respuesta = MessageBox.Show("¿Desea guardar la configuración de publicacion en el proyecto? ", "Archivo Configuracion", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (respuesta == DialogResult.Yes)
            {
                ArchivoConfiguracion.CrearArchivoConfiguracion(PublicacionConfig);
            }
        }

        private void CompletarCampos(PublicacionConfig publicacionConfig)
        {
            PublicacionConfig = publicacionConfig;

            txtCarpetaOutput.Text = PublicacionConfig.PathOutputLocal;
            txtCarpetaRemota.Text = PublicacionConfig.PathOutputRemota;
            txtPathArchivo.Text = PublicacionConfig.ArchivoProyectoPath;
            txtServidor.Text = PublicacionConfig.Servidor.ServidorAddress;
            txtUsuario.Text = PublicacionConfig.Servidor.Usuario;
            chkEliminarArchivosCarpetaOutput.Checked = PublicacionConfig.EliminarExistenteLocal;
            chkEliminarArchivosCarpetaRemota.Checked = PublicacionConfig.EliminarExistenteRemoto;

            
        }

        private void BtnSeleccionarCarpetaOutput_Click(object sender, EventArgs e)
        {
            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                txtCarpetaOutput.Text = folderDialog.SelectedPath;
            }

        }

        private void BtnPublicar_Click(object sender, EventArgs e)
        {

            if (Validaciones())
            {
                try
                {
                    PublicacionConfig _publicacionConfig = PublicacionConfig;
                    NewPublicacionConfig(ref _publicacionConfig);
                    PublicacionProyecto publicacionProyecto = new PublicacionProyecto(_publicacionConfig);






                    if (_publicacionConfig.ConfiguracionAvanzada.CrearArchivoNginx)
                    {
                        publicacionProyecto.CrearArchivoNginx();
                    }

                    if (_publicacionConfig.EliminarExistenteLocal && _publicacionConfig.ProyectoTipo == ProyectoTipoEnum.NetCore)
                    {
                        publicacionProyecto.EliminarArchivosLocales();
                    }
                    if (_publicacionConfig.ProyectoTipo == ProyectoTipoEnum.NetCore)
                    {
                        publicacionProyecto.CompilarProyecto();
                    }
                    

                    if (_publicacionConfig.EliminarExistenteRemoto)
                    {
                        publicacionProyecto.EliminarArchivosRemotos();
                    }
                    publicacionProyecto.PublicarProyecto();

                    if (_publicacionConfig.ConfiguracionAvanzada.CrearServicioLinux)
                    {
                        publicacionProyecto.CrearServicio();
                    }

                    MessageBox.Show("La publicación se logro exitosamente.", "Publicar", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                    if (_publicacionConfig.ProyectoTipo == ProyectoTipoEnum.NetCore)
                        GuardarArchivoConfiguracion(new FileInfo(_publicacionConfig.ArchivoProyectoPath).Directory);

                    if (_publicacionConfig.ProyectoTipo == ProyectoTipoEnum.NPM)
                        GuardarArchivoConfiguracion(new DirectoryInfo(_publicacionConfig.PathOutputLocal));

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Publicar", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            

        }

        private void chkEliminarArchivosCarpetaOutput_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnConfAvanzada_Click(object sender, EventArgs e)
        {
            PublicacionConfig _publicacionConfig = PublicacionConfig;
            NewPublicacionConfig(ref _publicacionConfig);
            FrmConfAvanzada frmConfAvanzada = new FrmConfAvanzada(_publicacionConfig);
            if (frmConfAvanzada.ShowDialog() == DialogResult.OK)
            {

                PublicacionConfig.ConfiguracionAvanzada = frmConfAvanzada.PublicacionConfig.ConfiguracionAvanzada;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {

                //PublicacionProyecto publicacionProyecto = new PublicacionProyecto();
                //publicacionProyecto.Servidor = Servidor;
                //publicacionProyecto.SubirArchivo();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Publicar", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private bool Validaciones()
        {
            if (string.IsNullOrWhiteSpace(txtServidor.Text))
            {
                MessageBox.Show("Debe ingresar el servidor.", "Publicar", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(txtUsuario.Text))
            {
                MessageBox.Show("Debe ingresar un usuario.", "Publicar", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(txtContraseña.Text))
            {
                MessageBox.Show("Debe ingresar la contraseña.", "Publicar", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(txtPathArchivo.Text) && cboTipoProyecto.SelectedItem.ToString() == ProyectoTipoEnum.NetCore.ToString())
            {
                MessageBox.Show("Debe seleccionar el archivo del proyecto.", "Publicar", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(txtCarpetaOutput.Text))
            {
                MessageBox.Show("Debe seleccionar la carpeta donde compilar localmente.", "Publicar", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(txtCarpetaRemota.Text))
            {
                MessageBox.Show("Debe seleccionar la carpeta donde publicar en el servidor.", "Publicar", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            }
            else
            {
                return true;
            }
        }

   
        private void cboTipoProyecto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboTipoProyecto.SelectedItem.ToString() == ProyectoTipoEnum.NetCore.ToString())
            {
                label4.Visible = true;
                txtPathArchivo.Visible = true;
                btnSeleccionarArchivo.Visible = true;
                chkEliminarArchivosCarpetaOutput.Visible = true;
            }
            else
            {
                label4.Visible = false;
                txtPathArchivo.Visible = false;
                btnSeleccionarArchivo.Visible = false;
                chkEliminarArchivosCarpetaOutput.Visible = false;
            }
        }

        
    }
}
