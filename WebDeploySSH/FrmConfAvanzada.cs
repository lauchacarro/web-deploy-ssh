﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebDeploySSH.DTO;

namespace WebDeploySSH
{
    public partial class FrmConfAvanzada : Form
    {

        public PublicacionConfig PublicacionConfig { get; private set; }

        public FrmConfAvanzada(PublicacionConfig publicacionConfig)
        {
            InitializeComponent();
            PublicacionConfig = publicacionConfig;
            if(PublicacionConfig.ConfiguracionAvanzada != null)
            {
                chkCrearArchivoNginx.Checked = PublicacionConfig.ConfiguracionAvanzada.CrearArchivoNginx;
                chkCrearServicioLinux.Checked = PublicacionConfig.ConfiguracionAvanzada.CrearServicioLinux;
                txtUrls.Lines = PublicacionConfig.ConfiguracionAvanzada.Urls;
                txtPuerto.Value = PublicacionConfig.ConfiguracionAvanzada.Puerto;
                switch (PublicacionConfig.ConfiguracionAvanzada.Ambiente)
                {
                    case Enums.AmbienteEnum.Test:
                        radTesting.Checked = true;
                        break;
                    case Enums.AmbienteEnum.Prod:
                        radProduccion.Checked = true;
                        break;
               
                }

            }
        }

        


        private const string MSGTitle = "Configuracion Avanzada";

        private void BtnAceptar_Click(object sender, EventArgs e)
        {

            if(!radProduccion.Checked && !radTesting.Checked)
            {
                MessageBox.Show("Debe seleccionar un ambiente.", MSGTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                return;
            }

            if (chkCrearArchivoNginx.Checked && string.IsNullOrWhiteSpace(txtUrls.Text))
            {
                MessageBox.Show("Debe ingresar un dominio.", MSGTitle , MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                return;
            }
            if (chkCrearArchivoNginx.Checked && txtPuerto.Value == 0)
            {
                MessageBox.Show("Debe ingresar un puerto local.", MSGTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                return;
            }
            PublicacionConfig.ConfiguracionAvanzada = new ConfiguracionAvanzada
            {
                Urls = txtUrls.Lines,
                CrearArchivoNginx = chkCrearArchivoNginx.Checked,
                CrearServicioLinux = chkCrearServicioLinux.Checked,
                Puerto = Convert.ToInt32(txtPuerto.Value),
                Ambiente = radTesting.Checked ? Enums.AmbienteEnum.Test : Enums.AmbienteEnum.Prod

            };

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void ChkCrearArchivoNginx_CheckedChanged(object sender, EventArgs e)
        {
            txtUrls.Enabled = chkCrearArchivoNginx.Checked;
            txtPuerto.Enabled = chkCrearArchivoNginx.Checked;
            txtUrls.Text = string.Empty;
        }
    }
}
