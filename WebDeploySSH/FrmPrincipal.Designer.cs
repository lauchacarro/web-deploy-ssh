﻿using WebDeploySSH.Enums;

namespace WebDeploySSH
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnValidarConexion = new System.Windows.Forms.Button();
            this.lblContraseña = new System.Windows.Forms.Label();
            this.txtContraseña = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.lblServidor = new System.Windows.Forms.Label();
            this.txtServidor = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboTipoProyecto = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnConfAvanzada = new System.Windows.Forms.Button();
            this.txtCarpetaRemota = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkEliminarArchivosCarpetaRemota = new System.Windows.Forms.CheckBox();
            this.chkEliminarArchivosCarpetaOutput = new System.Windows.Forms.CheckBox();
            this.btnPublicar = new System.Windows.Forms.Button();
            this.btnSeleccionarCarpetaOutput = new System.Windows.Forms.Button();
            this.txtCarpetaOutput = new System.Windows.Forms.TextBox();
            this.lblCarpetaOutput = new System.Windows.Forms.Label();
            this.folderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.txtPathArchivo = new System.Windows.Forms.TextBox();
            this.btnSeleccionarArchivo = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileDialog
            // 
            this.fileDialog.FileName = "openFileDialog1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnValidarConexion);
            this.groupBox1.Controls.Add(this.lblContraseña);
            this.groupBox1.Controls.Add(this.txtContraseña);
            this.groupBox1.Controls.Add(this.lblUsuario);
            this.groupBox1.Controls.Add(this.txtUsuario);
            this.groupBox1.Controls.Add(this.lblServidor);
            this.groupBox1.Controls.Add(this.txtServidor);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(292, 399);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Acceso Servidor";
            // 
            // btnValidarConexion
            // 
            this.btnValidarConexion.Location = new System.Drawing.Point(6, 339);
            this.btnValidarConexion.Name = "btnValidarConexion";
            this.btnValidarConexion.Size = new System.Drawing.Size(109, 34);
            this.btnValidarConexion.TabIndex = 4;
            this.btnValidarConexion.Text = "Validar Conexión";
            this.btnValidarConexion.UseVisualStyleBackColor = true;
            this.btnValidarConexion.Click += new System.EventHandler(this.BtnValidarConexion_Click);
            // 
            // lblContraseña
            // 
            this.lblContraseña.AutoSize = true;
            this.lblContraseña.Location = new System.Drawing.Point(6, 218);
            this.lblContraseña.Name = "lblContraseña";
            this.lblContraseña.Size = new System.Drawing.Size(64, 13);
            this.lblContraseña.TabIndex = 5;
            this.lblContraseña.Text = "Contraseña:";
            // 
            // txtContraseña
            // 
            this.txtContraseña.Location = new System.Drawing.Point(6, 234);
            this.txtContraseña.Name = "txtContraseña";
            this.txtContraseña.PasswordChar = '*';
            this.txtContraseña.Size = new System.Drawing.Size(256, 20);
            this.txtContraseña.TabIndex = 3;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(6, 168);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(46, 13);
            this.lblUsuario.TabIndex = 3;
            this.lblUsuario.Text = "Usuario:";
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(6, 184);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(256, 20);
            this.txtUsuario.TabIndex = 1;
            // 
            // lblServidor
            // 
            this.lblServidor.AutoSize = true;
            this.lblServidor.Location = new System.Drawing.Point(6, 92);
            this.lblServidor.Name = "lblServidor";
            this.lblServidor.Size = new System.Drawing.Size(49, 13);
            this.lblServidor.TabIndex = 1;
            this.lblServidor.Text = "Servidor:";
            // 
            // txtServidor
            // 
            this.txtServidor.Location = new System.Drawing.Point(6, 108);
            this.txtServidor.Name = "txtServidor";
            this.txtServidor.Size = new System.Drawing.Size(256, 20);
            this.txtServidor.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtPathArchivo);
            this.groupBox2.Controls.Add(this.btnSeleccionarArchivo);
            this.groupBox2.Controls.Add(this.cboTipoProyecto);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.btnConfAvanzada);
            this.groupBox2.Controls.Add(this.txtCarpetaRemota);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.chkEliminarArchivosCarpetaRemota);
            this.groupBox2.Controls.Add(this.chkEliminarArchivosCarpetaOutput);
            this.groupBox2.Controls.Add(this.btnPublicar);
            this.groupBox2.Controls.Add(this.btnSeleccionarCarpetaOutput);
            this.groupBox2.Controls.Add(this.txtCarpetaOutput);
            this.groupBox2.Controls.Add(this.lblCarpetaOutput);
            this.groupBox2.Location = new System.Drawing.Point(327, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(278, 399);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Publicar";
            // 
            // cboTipoProyecto
            // 
            this.cboTipoProyecto.FormattingEnabled = true;
            this.cboTipoProyecto.Items.AddRange(new object[] {
            "NetCore",
            "NPM"});
            this.cboTipoProyecto.Location = new System.Drawing.Point(6, 48);
            this.cboTipoProyecto.Name = "cboTipoProyecto";
            this.cboTipoProyecto.Size = new System.Drawing.Size(217, 21);
            this.cboTipoProyecto.TabIndex = 15;
            this.cboTipoProyecto.Text = "NetCore";
            this.cboTipoProyecto.SelectedIndexChanged += new System.EventHandler(this.cboTipoProyecto_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Tipo de Proyecto";
            // 
            // btnConfAvanzada
            // 
            this.btnConfAvanzada.Location = new System.Drawing.Point(10, 340);
            this.btnConfAvanzada.Name = "btnConfAvanzada";
            this.btnConfAvanzada.Size = new System.Drawing.Size(170, 34);
            this.btnConfAvanzada.TabIndex = 12;
            this.btnConfAvanzada.Text = "Configuración Avanzada";
            this.btnConfAvanzada.UseVisualStyleBackColor = true;
            this.btnConfAvanzada.Click += new System.EventHandler(this.btnConfAvanzada_Click);
            // 
            // txtCarpetaRemota
            // 
            this.txtCarpetaRemota.Location = new System.Drawing.Point(10, 219);
            this.txtCarpetaRemota.Name = "txtCarpetaRemota";
            this.txtCarpetaRemota.Size = new System.Drawing.Size(220, 20);
            this.txtCarpetaRemota.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Carpeta Output Remota";
            // 
            // chkEliminarArchivosCarpetaRemota
            // 
            this.chkEliminarArchivosCarpetaRemota.AutoSize = true;
            this.chkEliminarArchivosCarpetaRemota.Location = new System.Drawing.Point(10, 297);
            this.chkEliminarArchivosCarpetaRemota.Name = "chkEliminarArchivosCarpetaRemota";
            this.chkEliminarArchivosCarpetaRemota.Size = new System.Drawing.Size(205, 17);
            this.chkEliminarArchivosCarpetaRemota.TabIndex = 11;
            this.chkEliminarArchivosCarpetaRemota.Text = "Eliminar archivos de la carpeta remota";
            this.chkEliminarArchivosCarpetaRemota.UseVisualStyleBackColor = true;
            // 
            // chkEliminarArchivosCarpetaOutput
            // 
            this.chkEliminarArchivosCarpetaOutput.AutoSize = true;
            this.chkEliminarArchivosCarpetaOutput.Location = new System.Drawing.Point(10, 260);
            this.chkEliminarArchivosCarpetaOutput.Name = "chkEliminarArchivosCarpetaOutput";
            this.chkEliminarArchivosCarpetaOutput.Size = new System.Drawing.Size(242, 17);
            this.chkEliminarArchivosCarpetaOutput.TabIndex = 10;
            this.chkEliminarArchivosCarpetaOutput.Text = "Eliminar archivos existentes de carpeta output";
            this.chkEliminarArchivosCarpetaOutput.UseVisualStyleBackColor = true;
            this.chkEliminarArchivosCarpetaOutput.CheckedChanged += new System.EventHandler(this.chkEliminarArchivosCarpetaOutput_CheckedChanged);
            // 
            // btnPublicar
            // 
            this.btnPublicar.Location = new System.Drawing.Point(186, 340);
            this.btnPublicar.Name = "btnPublicar";
            this.btnPublicar.Size = new System.Drawing.Size(75, 34);
            this.btnPublicar.TabIndex = 13;
            this.btnPublicar.Text = "Publicar";
            this.btnPublicar.UseVisualStyleBackColor = true;
            this.btnPublicar.Click += new System.EventHandler(this.BtnPublicar_Click);
            // 
            // btnSeleccionarCarpetaOutput
            // 
            this.btnSeleccionarCarpetaOutput.Location = new System.Drawing.Point(236, 167);
            this.btnSeleccionarCarpetaOutput.Name = "btnSeleccionarCarpetaOutput";
            this.btnSeleccionarCarpetaOutput.Size = new System.Drawing.Size(36, 20);
            this.btnSeleccionarCarpetaOutput.TabIndex = 7;
            this.btnSeleccionarCarpetaOutput.Text = "...";
            this.btnSeleccionarCarpetaOutput.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSeleccionarCarpetaOutput.UseVisualStyleBackColor = true;
            this.btnSeleccionarCarpetaOutput.Click += new System.EventHandler(this.BtnSeleccionarCarpetaOutput_Click);
            // 
            // txtCarpetaOutput
            // 
            this.txtCarpetaOutput.Location = new System.Drawing.Point(10, 168);
            this.txtCarpetaOutput.Name = "txtCarpetaOutput";
            this.txtCarpetaOutput.Size = new System.Drawing.Size(220, 20);
            this.txtCarpetaOutput.TabIndex = 8;
            // 
            // lblCarpetaOutput
            // 
            this.lblCarpetaOutput.AutoSize = true;
            this.lblCarpetaOutput.Location = new System.Drawing.Point(10, 152);
            this.lblCarpetaOutput.Name = "lblCarpetaOutput";
            this.lblCarpetaOutput.Size = new System.Drawing.Size(108, 13);
            this.lblCarpetaOutput.TabIndex = 3;
            this.lblCarpetaOutput.Text = "Carpeta Output Local";
            // 
            // txtPathArchivo
            // 
            this.txtPathArchivo.Location = new System.Drawing.Point(6, 108);
            this.txtPathArchivo.Name = "txtPathArchivo";
            this.txtPathArchivo.Size = new System.Drawing.Size(223, 20);
            this.txtPathArchivo.TabIndex = 12;
            // 
            // btnSeleccionarArchivo
            // 
            this.btnSeleccionarArchivo.Location = new System.Drawing.Point(235, 108);
            this.btnSeleccionarArchivo.Name = "btnSeleccionarArchivo";
            this.btnSeleccionarArchivo.Size = new System.Drawing.Size(36, 20);
            this.btnSeleccionarArchivo.TabIndex = 11;
            this.btnSeleccionarArchivo.Text = "...";
            this.btnSeleccionarArchivo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSeleccionarArchivo.UseVisualStyleBackColor = true;
            this.btnSeleccionarArchivo.Click += new System.EventHandler(this.BtnSeleccionarArchivo_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Ubicación Archivo csproj";
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 450);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmPrincipal";
            this.Text = "Web Deploy SSH";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog fileDialog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnValidarConexion;
        private System.Windows.Forms.Label lblContraseña;
        private System.Windows.Forms.TextBox txtContraseña;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.Label lblServidor;
        private System.Windows.Forms.TextBox txtServidor;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnPublicar;
        private System.Windows.Forms.Button btnSeleccionarCarpetaOutput;
        private System.Windows.Forms.TextBox txtCarpetaOutput;
        private System.Windows.Forms.Label lblCarpetaOutput;
        private System.Windows.Forms.FolderBrowserDialog folderDialog;
        private System.Windows.Forms.CheckBox chkEliminarArchivosCarpetaRemota;
        private System.Windows.Forms.CheckBox chkEliminarArchivosCarpetaOutput;
        private System.Windows.Forms.TextBox txtCarpetaRemota;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnConfAvanzada;
        private System.Windows.Forms.ComboBox cboTipoProyecto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPathArchivo;
        private System.Windows.Forms.Button btnSeleccionarArchivo;
        private System.Windows.Forms.Label label4;
    }
}

