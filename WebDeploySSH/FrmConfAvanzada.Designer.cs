﻿namespace WebDeploySSH
{
    partial class FrmConfAvanzada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPuerto = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.txtUrls = new System.Windows.Forms.TextBox();
            this.lblDominio = new System.Windows.Forms.Label();
            this.chkCrearServicioLinux = new System.Windows.Forms.CheckBox();
            this.chkCrearArchivoNginx = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radTesting = new System.Windows.Forms.RadioButton();
            this.radProduccion = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPuerto)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.txtPuerto);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnAceptar);
            this.groupBox1.Controls.Add(this.txtUrls);
            this.groupBox1.Controls.Add(this.lblDominio);
            this.groupBox1.Controls.Add(this.chkCrearServicioLinux);
            this.groupBox1.Controls.Add(this.chkCrearArchivoNginx);
            this.groupBox1.Location = new System.Drawing.Point(43, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(401, 399);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Configuración";
            // 
            // txtPuerto
            // 
            this.txtPuerto.Location = new System.Drawing.Point(243, 235);
            this.txtPuerto.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
            this.txtPuerto.Name = "txtPuerto";
            this.txtPuerto.Size = new System.Drawing.Size(120, 20);
            this.txtPuerto.TabIndex = 7;
            this.txtPuerto.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(240, 205);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Puerto Local";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(20, 352);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 4;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.BtnAceptar_Click);
            // 
            // txtUrls
            // 
            this.txtUrls.Enabled = false;
            this.txtUrls.Location = new System.Drawing.Point(17, 234);
            this.txtUrls.Multiline = true;
            this.txtUrls.Name = "txtUrls";
            this.txtUrls.Size = new System.Drawing.Size(201, 88);
            this.txtUrls.TabIndex = 3;
            // 
            // lblDominio
            // 
            this.lblDominio.AutoSize = true;
            this.lblDominio.Location = new System.Drawing.Point(17, 205);
            this.lblDominio.Name = "lblDominio";
            this.lblDominio.Size = new System.Drawing.Size(34, 13);
            this.lblDominio.TabIndex = 2;
            this.lblDominio.Text = "URLs";
            // 
            // chkCrearServicioLinux
            // 
            this.chkCrearServicioLinux.AutoSize = true;
            this.chkCrearServicioLinux.Location = new System.Drawing.Point(17, 33);
            this.chkCrearServicioLinux.Name = "chkCrearServicioLinux";
            this.chkCrearServicioLinux.Size = new System.Drawing.Size(118, 17);
            this.chkCrearServicioLinux.TabIndex = 1;
            this.chkCrearServicioLinux.Text = "Crear servicio Linux";
            this.chkCrearServicioLinux.UseVisualStyleBackColor = true;
            // 
            // chkCrearArchivoNginx
            // 
            this.chkCrearArchivoNginx.AutoSize = true;
            this.chkCrearArchivoNginx.Location = new System.Drawing.Point(17, 76);
            this.chkCrearArchivoNginx.Name = "chkCrearArchivoNginx";
            this.chkCrearArchivoNginx.Size = new System.Drawing.Size(201, 17);
            this.chkCrearArchivoNginx.TabIndex = 0;
            this.chkCrearArchivoNginx.Text = "Crear archivo de configuración Nginx";
            this.chkCrearArchivoNginx.UseVisualStyleBackColor = true;
            this.chkCrearArchivoNginx.CheckedChanged += new System.EventHandler(this.ChkCrearArchivoNginx_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radProduccion);
            this.groupBox2.Controls.Add(this.radTesting);
            this.groupBox2.Location = new System.Drawing.Point(17, 112);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(346, 68);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ambiente";
            // 
            // radTesting
            // 
            this.radTesting.AutoSize = true;
            this.radTesting.Location = new System.Drawing.Point(38, 31);
            this.radTesting.Name = "radTesting";
            this.radTesting.Size = new System.Drawing.Size(60, 17);
            this.radTesting.TabIndex = 0;
            this.radTesting.TabStop = true;
            this.radTesting.Text = "Testing";
            this.radTesting.UseVisualStyleBackColor = true;
            // 
            // radProduccion
            // 
            this.radProduccion.AutoSize = true;
            this.radProduccion.Location = new System.Drawing.Point(211, 31);
            this.radProduccion.Name = "radProduccion";
            this.radProduccion.Size = new System.Drawing.Size(79, 17);
            this.radProduccion.TabIndex = 1;
            this.radProduccion.TabStop = true;
            this.radProduccion.Text = "Producción";
            this.radProduccion.UseVisualStyleBackColor = true;
            // 
            // FrmConfAvanzada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 464);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmConfAvanzada";
            this.Text = "Configuración Avanzada";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPuerto)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkCrearServicioLinux;
        private System.Windows.Forms.CheckBox chkCrearArchivoNginx;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.TextBox txtUrls;
        private System.Windows.Forms.Label lblDominio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txtPuerto;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radProduccion;
        private System.Windows.Forms.RadioButton radTesting;
    }
}