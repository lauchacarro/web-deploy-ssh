﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDeploySSH.DTO
{
    public class Servidor
    {
        public string ServidorAddress { get; set; }

        public string Usuario { get; set; }

        public string Contraseña { get; set; }
    }
}
