﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDeploySSH.Enums;

namespace WebDeploySSH.DTO
{
    public class PublicacionConfig
    {
        public string ArchivoProyectoPath { get; set; }

        public string PathOutputLocal { get; set; }

        public string PathOutputRemota { get; set; }

        public bool EliminarExistenteLocal { get; set; }

        public bool EliminarExistenteRemoto { get; set; }

        public ProyectoTipoEnum ProyectoTipo { get; set; }

        public Servidor Servidor { get; set; }

        public ConfiguracionAvanzada ConfiguracionAvanzada {get; set;}

    }
}
