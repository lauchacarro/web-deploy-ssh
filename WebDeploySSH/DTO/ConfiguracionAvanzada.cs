﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDeploySSH.Enums;

namespace WebDeploySSH.DTO
{
    public class ConfiguracionAvanzada
    {
        public bool CrearArchivoNginx { get; set; }

        public bool CrearServicioLinux { get; set; }

        public string[] Urls { get; set; }

        public AmbienteEnum Ambiente { get; set; }

        public int Puerto { get; set; }
    }
}
